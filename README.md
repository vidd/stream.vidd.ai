# stream.vidd.ai #

>Use stream.vidd.ai to stream batches of footage files to the vidd.ai server. It's especially useful when you have large files (>4GB each).

>[Vidd.ai](http://vidd.ai) generates a summary of what's inside your raw footage
so you won't have to rummage to find segments to grab and edit.

-----------------------
### About this repository ###

>stream.vidd.ai uses just your *browser and ffmpeg* to break down large file into small chunk __in parallel__ to send to the vidd.ai server.
>Large files can be > 4GB. Benchmarks suggest 1 GB of footage takes about a minute to send.

>* Current version: `Mac 1.0`

-----------------------
### Installing ###

>Paste this command into your Terminal window

>	```curl 'http://stream.vidd.ai/stream_setup.sh' > stream_setup.sh; ksh stream_setup.sh
	```

-----------------------
### Inside the setup script ###

>The script will:

>1. install ffmpeg (if you don't already have it) (it will also install Homebrew if you don't already have it)

>2. configure Apache

>3. create an empty log file

>4. open your default browser with the stream.vidd.ai front-end

-----------------------
### Issues? ###

> founder.vidd.ai at gmail
