#!/bin/ksh

# check if already have ffmpeg
# usually /usr/local/bin/ffmpeg
is_ffmpeg=`which ffmpeg`
if [ -z $is_ffmpeg ]
then
	# check if already have Homebrew
	# usually in /usr/local/bin/brew

	is_brew=`which brew`
	if [ -z $is_brew ]
	then
		echo installing Homebrew
		/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	else
		echo located Homebrew
	fi
	echo installing ffmpeg
	brew install ffmpeg
else
	echo located ffmpeg
fi


cd
user=`whoami`
echo $user
home_dir=`pwd`

# clone stream.vidd.ai
is_git=`ls stream.vidd.ai`
if [ -z $is_git ]
then
	echo cloning stream.vidd.ai from bitbucket into $home_dir
	git clone https://authentiq_bb@bitbucket.org/viddai/stream.vidd.ai.git
	# cd to streaming dir
	cd stream.vidd.ai
else
	# cd to streaming dir
	echo stream.vidd.ai already exists, doing a git pull
	cd stream.vidd.ai
	git pull
fi

# if httpd.conf already exists, overwrite local copy
if [ -f /etc/apache2/httpd.conf ]
then
	echo copying httpd.conf
	cp /etc/apache2/httpd.conf stream.httpd.conf
fi

# add module activations
echo adding activations to stream.httpd.conf
cat stream.httpd.conf.activations >> stream.httpd.conf

# set some vars
echo setting vars for stream.httpd.conf and stream.httpd-userdir.conf
sed "s:HOME_DIR:$home_dir:" < stream.httpd.conf > stream.httpd.conf.new
sed "s:HOME_DIR:$home_dir:" < stream.httpd-userdir.conf > stream.httpd-userdir.conf.new

echo restarting apache with new config
sudo apachectl -k restart -D "HOME $home_dir" -f $home_dir/stream.vidd.ai/stream.httpd.conf.new

# create _q.log
echo creating _q.log
> _q.log
chmod 666 _q.log

# open browser
url='http://localhost/~'$user'/resizeVideo.php#'
open $url
echo stream.vidd.ai setup complete, use your browser to stream your footage files to our server

